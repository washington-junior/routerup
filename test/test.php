<?php

error_reporting(E_ALL);

require_once __DIR__ .'/../vendor/autoload.php';

use Router\Response;
use Router\Request;
use Router\Router;

echo '<pre>';

$app = new Router();

$app->add(function(Request $req, Response $res) {
  echo getenv('REDIRECT_APP_USER') . PHP_EOL;
});

$app->get('index', function(Request $req, Response $res) {
  echo $req->instance;
  echo 'Index';
});

$app->get('about', function(Request $req, Response $res) {
  echo 'About';
});

$app->group('ajax', function(Router $app) {

  $app->get('list', function(Request $req, Response $res, $next) {
    echo 'Ajax List';
  });
  $app->get('add', function(Request $req, Response $res) {
    echo 'Ajax Add';
  });

  $app->group(':id', function(Router $app) {

    $app->get('index', function(Request $req, Response $res) {
      echo "Ajax Index of {$req->params->id}";
    });
    $app->get('details', function(Request $req, Response $res) {
      echo "Ajax Details of {$req->params->id}";
    });

  });

});

/*

Router
  prefix
  config

  group($path) {
    $app = new Router($path, $this->config);
  }


*/
