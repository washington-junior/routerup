<?php

namespace Router;

use Braintree\Exception;

/**
 * Request
 */
class Request
{

    public $path;
    public $params;
    public $body;
    public $query;
    public $instance;
    public $post;
    public $files;
    public $session;

    /**
     * @param $path
     */
    function __construct($path)
    {
        $this->path = $path;
        $this->query = $_GET;
        $this->post = (object)$_POST;
        $this->files = (object)$_FILES;
        $this->body = json_decode(file_get_contents('php://input'));
    }

    /**
     * @param $index
     * @param $value
     * @throws Exception
     */
    function addSessionField($index, $value)
    {
        session_start();
        if (!isset($_SESSION[$index])) {
            $_SESSION[$index] = $value;
        } else {
            throw new Exception('SESSION IN INDEX ' . $index . 'CAN NOT BE REPLACE');
        }
    }

    function getSessionField($index)
    {
        return isset($_SESSION[$index]) ? $_SESSION[$index] : false;
    }
}
