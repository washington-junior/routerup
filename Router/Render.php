<?
namespace Router;

use Twig_Environment;
use Twig_Extension_Debug;
use Twig_Loader_Filesystem;

Class Render
{

    /**
     * Render constructor.
     * @param string $viewPath
     * @param string $viewCache
     */
    private $renderFile;
    private $cachePath;

    public function __construct($viewPath, $debug, $viewCache = false)
    {
        $loader = new Twig_Loader_Filesystem($viewPath);
        $this->cachePath = $viewCache;
        $twigConfig = [];
        if ($this->cachePath) {
            $twigConfig['cache'] = $this->cachePath;
        }
        $twigConfig['debug'] = $debug;
        $this->renderFile = new Twig_Environment($loader, $twigConfig);
        $this->enableDump();
    }

    public function render($fileName, $data = [])
    {

        if (count($data) > 0) {
            echo $this->renderFile->render($fileName, $data);
        } else {
            echo $this->renderFile->render($fileName);
        }
    }

    public function enableDump()
    {
        $this->renderFile->addExtension(new Twig_Extension_Debug());
    }

    public function mailTemplate($template, $data)
    {
        if (count($data) > 0) {
            return $this->renderFile->render($template, $data);
        } else {
            return $this->renderFile->render($template);
        }
    }
}