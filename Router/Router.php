<?php

namespace Router;

use Router\Config;
use Router\Request;
use Router\Response;

/**
 *    Utility for parsing friendly URLs and work with slugs
 */
class Router extends App
{
    /**
     * Router constructor.
     * @param App $app
     * @param String $prefix
     */
    public function __construct($prefix, App $app)
    {
        $this->prefix = $this->normalize($prefix);
        $this->request = $app->request;
        $this->response = $app->response;
        $this->config = $app->config;
        $this->publicPaths = $app->publicPaths;
        $this->publicPathsDir = $app->publicPathsDir;
    }
}
