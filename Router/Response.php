<?php

namespace Router;

/**
 *
 */
class Response
{

    public $app;

    function __construct(App $app)
    {
        $this->app = $app;
    }

    public function write($ouput)
    {
        if (is_array($ouput)) {
            echo '<pre>';
            print_r($ouput);
        } else {
            echo $ouput;
        }
    }

    public function json($json, $inJSON = false)
    {
        $this->withHeader('Content-type', 'application/json');
        $this->withHeader('Cache-Control', 'max-age=604800000');
        echo($inJSON ? $json : json_encode($json));
    }

    public function withHeader($headerName, $headerValue)
    {
        header($headerName . ': ' . $headerValue);
    }

    /**
     * @param int $statusCode http status code, like 200,404,501
     */
    public function withStatus($statusCode = 200)
    {
        http_response_code($statusCode);
    }

    /**
     * @param $template
     * @param $data
     */
    public function render($template, $data)
    {
        $this->app->config->templateEngine->render($template, $data);
    }

    public function mailTemplate($template, $data)
    {
        return $this->app->config->templateEngine->mailTemplate($template, $data);
    }

    public function customView($view, $data = array())
    {
        $params = $data;
        unset($data);
        include $view;
    }
}
