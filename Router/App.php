<?php

namespace Router;

use finfo;
use Gregwar\Image\Image;
use Mimey\MimeTypes;
use Router\Config;
use Router\Request;
use Router\Response;

/**
 *    Utility for parsing friendly URLs and work with slugs
 */
class App
{

    public $prefix;
    public $config;
    public $ended = FALSE;
    public $request;
    public $response;
    public $routeFounded;
    public $session;
    public $publicPaths = [];
    public $publicPathsDir = [];

    function __construct($prefix = '', $config = false)
    {
        $this->prefix = $this->normalize($prefix);

        if (!$config) {
            $this->config = new Config($this);
        }
        $requestPath = $_SERVER['PATH_INFO'];
        $path = $this->normalize($requestPath);

        if ('/' . $path !== $requestPath && $path !== '/') {
            header('Location: ..');
            die;
        }
        $this->request = new Request($path);
        $this->response = new Response($this);
    }

    public function onShutdown()
    {
        if (!$this->routeFounded) {
            header("Status: 404 Not Found");
            echo 'Erro 404';
        }
    }

    /**
     * @return boolean
     */
    public function next()
    {
        $this->ended = FALSE;
    }

    /**
     * @param  string $path is a regex string
     * @return string
     */
    public function normalize($path)
    {
        return preg_replace('/(^\/)|((\/)?(index)?$)/', '', $path);
    }

    /**
     * @param  string $str
     * @return boolean
     */
    public function isRegex($str)
    {
        $regex = '/^\/[\s\S]+\/$/';
        return preg_match($regex, $str);
    }

    /**
     * regExFromPath: Gera uma expressão para opter parametros do path
     * Transforma ":token/painel/produtos/:id"
     * Em "/(?P<token>\w+)\/painel\/produtos\/(?P<produto>\w+)/"
     * @param  string $path URL path
     * @return string       Regular expression
     */
    public function regExFromPath($path)
    {
        $matches = null;
        $pathExp = preg_quote($this->normalize($path), '/');

        if (preg_match_all('/\\\\:\w+/', $pathExp, $matches)) {
            foreach ($matches[0] as $key => $value) {
                $param = trim($value, '\\\\:');
                $pathExp = str_replace($value, '(?P<' . $param . '>[\\w-_]+)', $pathExp);
            }
        }
        return '/^' . $pathExp . '$/';
    }

    /**
     * Executa $callback(), quando o METHOD e PATH do request HTTP forem os
     * especifiados em $method e $path
     * @param  string $method HTTP method
     * @param  string $path URL path template or RegExp
     * @param  $callback
     * @return bool If matches
     */
    public function match($method, $path, $callback)
    {
        if ($_SERVER['REQUEST_METHOD'] !== $method) {
            return false;
        }
        $matches = null;
        if ($this->isRegex($path)) {
            $regEx = $path;
        } else {
            $test = $this->normalize($this->prefix . '/' . $path);
            $regEx = $this->regExFromPath($test);
        }
        $match = preg_match($regEx, $this->request->path, $matches);

        if (!$match) {
            return false;
        }

        $this->request->params = (object)$matches;

        $this->ended = $this->routeFounded = true;
        header('X-Powered-By: ViewUP (http://viewup.com.br) ', true, 200);
        $callback($this->request, $this->response, [$this, 'next']);
        if ($this->ended) {
            die;
        }
        return false;
    }

    public function setStatic($callback)
    {
        $path = explode('/', $_SERVER['PATH_INFO']);
        foreach ($path as $item) {
            if (in_array($item, $this->getStaticPath())) {
                $path = $item;
                continue;
            }
        }
        header('X-Powered-By: ViewUP (http://viewup.com.br) ', true, 200);
        if (in_array($path, $this->getStaticPath())) {

            $pD = $this->getPublicPathsDir()[$path] ? $this->getPublicPathsDir()[$path] : '/public';
            $arr = explode($path, $_SERVER['REDIRECT_URL']);

            $file = __DIR__ . '/../../../../' . $path. '/' . $pD . $arr[1];
            if (is_file($file)) {
                $type = explode('/', $arr[1]);
                $cont = count($type) - 1;
                $type = explode('.', $type[$cont]);
                $cont = count($type) - 1;
                $type = $type[$cont];
                $mimes = new \Mimey\MimeTypes;
                header('content-type: ' . $mimes->getMimeType($type));
                echo file_get_contents($file);
            } else {
                header("HTTP/1.1 404 Not Found");
                $tmp = explode('/', $file);
                $cont = count($tmp) - 1;
                header('File:' . $tmp[$cont]);
                echo 'not found file in folder ' . $path;
            }
            die;
        } else {
            $callback();
        }
    }

    public function setUserMediaSource()
    {
        header('X-Powered-By: ViewUP (http://viewup.com.br) ', true, 200);
        $arr = explode('gallery', $_SERVER['REDIRECT_URL']);
        if (count($arr) >= 2) {
            $file = __DIR__ . '/../../../../' . $this->request->instance->galleryPath . $arr[1];
            if (is_file($file)) {
                $type = explode('/', $arr[1]);
                $cont = count($type) - 1;
                $type = explode('.', $type[$cont]);
                $cont = count($type) - 1;
                $type = $type[$cont];
                $mimes = new \Mimey\MimeTypes;
                header('content-type: ' . $mimes->getMimeType($type));
                echo file_get_contents($file);
            } else {
                header("HTTP/1.1 404 Not Found");
                $tmp = explode('/', $file);
                $cont = count($tmp) - 1;
                header('File :' . $tmp[$cont]);
                echo 'File Not Found';
            }
            die;
        }
    }

    public function setStaticPath($publicPaths)
    {
        $this->publicPaths = $publicPaths;
    }

    public function getStaticPath()
    {
        return $this->publicPaths;
    }

    public function group($prefix, $callback)
    {
        $app = new Router($this->prefix . '/' . $prefix, $this);
        $callback($app);
    }

    /**
     * Atalho para match('GET', ...)
     * @param  string $path HTTP path pattern
     * @param  $callback
     */
    public function get($path, $callback)
    {
        $this->setStatic(function () use ($path, $callback) {
            $this->match('GET', $path, $callback);
        });
    }

    /**
     * Atalho para match('DELETE', ...)
     * @param  string $path HTTP path pattern
     * @param  $callback
     */
    public function delete($path, $callback)
    {
        $this->match('DELETE', $path, $callback);
    }

    /**
     * Atalho para match('POST', ...)
     * @param  string $path HTTP path pattern
     * @param  $callback
     */
    public function post($path, $callback)
    {
        $this->match('POST', $path, $callback);
    }

    public function put($path, $callback)
    {
        $this->match('PUT', $path, $callback);
    }

    /**
     * Função para adicionar middleware
     * @param $callback
     */
    public function add($callback)
    {
        $callback($this->request, $this->response);
    }

    /**
     * @return array
     */
    public function getPublicPathsDir()
    {
        return $this->publicPathsDir;
    }

    /**
     * @param array $publicPathsDir
     */
    public function setPublicPathsDir($publicPathsDir)
    {
        $this->publicPathsDir = $publicPathsDir;
    }

}
