# View Up PHP Router
View Up PHP Router is a library for handle requests by method and path.

## Install

Have composer installed, initialize a project and update your `composer.json`.
```json
{
  "name": "viewup/projeto",
  "require": {
    "viewup/router": "dev-master"
  },
  "repositories": [
    {
      "type": "vcs",
      "url":  "git@192.168.25.2:viewup/router.git"
    }
  ]
}
```

Run `composer install` or `composer update`.

Include on your project.
```php
<?php

require_once __DIR__ .'/vendor/autoload.php';

$app = new Router\Router();
```

## Example

```php
<?php

require_once __DIR__ .'/vendor/autoload.php';

$app = new Router\Router();

// All URLs
$app->add(function($req, $res) {
  echo '<pre>';
  echo $_SERVER['PATH_INFO'] . PHP_EOL;
});

// For example.com/
$app->get('index', function($req, $res) {
  echo $req->instance;
  echo 'Index';
});

// For example.com/about
$app->get('about', function($req, $res) {
  echo 'About';
});

// Group with prefix ajax
$app->group('ajax', function(Router $app) {

  // For example.com/ajax/list
  $app->get('list', function($req, $res) {
    echo 'Ajax List';
  });
  // For example.com/ajax/add
  $app->get('add', function($req, $res) {
    echo 'Ajax Add';
  });

  // Group with prefix :id
  $app->group(':id', function(Router $app) {

    // For example.com/ajax/1432
    $app->get('index', function($req, $res) {
      echo "Ajax Index of {$req->params->id}";
    });
    // For example.com/ajax/1432/details
    $app->get('details', function($req, $res) {
      echo "Ajax Details of {$req->params->id}";
    });

  });

});

```
